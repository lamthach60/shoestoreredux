import logo from "./logo.svg";
import "./App.css";
import Ex_ShoeShop_Redux from "./ExerciseShoeShop_Redux/Ex_ShoeShop_Redux";

function App() {
  return <Ex_ShoeShop_Redux />;
}

export default App;
