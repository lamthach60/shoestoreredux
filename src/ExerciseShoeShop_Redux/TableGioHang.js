import React, { Component } from "react";
import { connect } from "react-redux";
import {
  REMOVE_TO_CART,
  CHANGE_QUANTITY,
} from "./redux/constant/shoeShopConstant";
class TableGioHang extends Component {
  renderContent = () => {
    return this.props.cart.map((item) => {
      return (
        <tr key={item.id}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleChange(-1);
              }}
            >
              -
            </button>
            <span className="mx-5">{item.soLuong}</span>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleChange(1);
              }}
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemove(item.id);
              }}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderContent()}</tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    cart: state.shoeShopReducer.gioHang,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleRemove: (id) => {
      dispatch({
        type: REMOVE_TO_CART,
        payload: id,
      });
    },
    handleChange: (step) => {
      dispatch({
        type: CHANGE_QUANTITY,
        payload: step,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TableGioHang);
