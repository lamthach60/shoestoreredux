import React, { Component } from "react";

import { shoeArr } from "./data_shoeshop";
import ItemShoe from "./ItemShoe";
import ListShoe from "./ListShoe";
import TableGioHang from "./TableGioHang";
class Ex_ShoeShop_Redux extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [shoeArr[0]],
  };

  handleAddToCart = (sp) => {
    // let cloneGioHang = [...this.state.gioHang, shoe]; //clone với push code es6
    // // ngoặc vuông là array, ngoặc nhọn là object
    // this.setState({ gioHang: cloneGioHang });

    let viTri = this.state.gioHang.findIndex((item) => {
      return item.id == sp.id;
    });
    let cloneGioHang = [...this.state.gioHang]; // phải clone array ko nên chỉnh sửa trực tiếp
    if (viTri == -1) {
      let newSP = { ...sp, soLuong: 1 };
      cloneGioHang.push(newSP);
    } else {
      cloneGioHang[viTri].soLuong++;
    }
    this.setState({ gioHang: cloneGioHang });
  };
  handleRemoveShoes = (id) => {
    let viTri = this.state.gioHang.findIndex((item) => {
      return item.id == id;
    });

    if (viTri !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(viTri, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };
  handleChangeQuantity = (idShoes, step) => {
    let viTri = this.state.gioHang.findIndex((item) => {
      return item.id == idShoes;
    });
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang[viTri].soLuong = cloneGioHang[viTri].soLuong + step;
    if (cloneGioHang[viTri].soLuong == 0) {
      cloneGioHang.splice(viTri, 1);
    }
    this.setState({ gioHang: cloneGioHang });
    console.log("viTri: ", viTri);
  };
  render() {
    return (
      <>
        <div className="container py-5">
          {/* {this.state.gioHang.length > 0 && ( */}
          <TableGioHang />
          {/* )} */}
          {/* render table */}
          <ListShoe />
        </div>
      </>
    );
  }
}
export default Ex_ShoeShop_Redux;
