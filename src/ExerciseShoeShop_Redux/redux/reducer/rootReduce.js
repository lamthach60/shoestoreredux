import { combineReducers } from "redux";
import { shoeShopReducer } from "./shoeShopReducer";
export let rootReduce_shoeShop = combineReducers({
  shoeShopReducer,
});
