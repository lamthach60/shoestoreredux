import { shoeArr } from "../../data_shoeshop";
import {
  ADD_TO_CART,
  CHANGE_QUANTITY,
  REMOVE_TO_CART,
} from "../constant/shoeShopConstant";

let intialState = {
  shoeArr: shoeArr,
  gioHang: [],
};
export let shoeShopReducer = (state = intialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let viTri = state.gioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      let cloneGioHang = [...state.gioHang]; // phải clone array ko nên chỉnh sửa trực tiếp
      if (viTri == -1) {
        let newSP = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSP);
      } else {
        cloneGioHang[viTri].soLuong++;
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case REMOVE_TO_CART: {
      let viTri = state.gioHang.findIndex((item) => {
        return item.id == payload;
      });
      let cloneGioHang = [...state.gioHang];
      if (viTri !== -1) {
        cloneGioHang.splice(viTri, 1);
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case CHANGE_QUANTITY: {
      let viTri = state.gioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      let cloneGioHang = [...state.gioHang]; // phải clone array ko nên chỉnh sửa trực tiếp
      if (viTri == -1) {
        let newSP = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSP);
      }
    }

    default:
      return state;
  }
};
